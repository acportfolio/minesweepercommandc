#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include <time.h>

#define MAXSIZE 34 //change this to effect max board size

int xSize = 0;
int ySize = 0;
int mineAmount = 0;

void InitalizeMineDistances(int mineDistance[xSize][ySize]) /// initalize the board with all zero from mine distances
{
  for(int x = 0; x < xSize; x++)
  {
    for(int y = 0; y < ySize; y++)
    {
      mineDistance[x][y] = 0;
    }
  }
}

void GenerateMineCords(int *xCor, int *yCor, int mineDistance[xSize][ySize]) //generate all the co-ordinates of the mines
{
  do
  {
    *xCor = rand() % xSize;
    *yCor = rand() % ySize;
  } while(mineDistance[*xCor][*yCor] == -1); // if mine already at said co-ordinates re-generate
}

void PlaceMines(int mineAmount, int mineDistance[xSize][ySize])
{
  int xCor;
  int yCor;

  for(int amount = 0; amount < mineAmount; amount++) // for number of mines generate co-ords and place them
  {
    xCor = 0;
    yCor = 0;
    GenerateMineCords(&xCor, &yCor, mineDistance);
    mineDistance[xCor][yCor] = -1; // -1 marks a mine
  }
}

void InitalizeKnownGrid(char valueKnown[xSize][ySize]) ///set the board to all X's to begin as nothing is known, no sweeping has occured
{
  for(int x = 0; x < xSize; x++)
  {
    for(int y = 0; y < ySize; y++)
    {
      valueKnown[x][y] = 'X';
    }
  }
}

int GetHowMany(int x, int y, int mineDistance[xSize][ySize]) //get how many mines are with the vicinty of a location
{
  int no = 0;
  int xCheck = x - 1;
  int yCheck = y - 1;

  do
  {
    do
    {
      if(xCheck < xSize && xCheck >= 0 && yCheck < ySize && yCheck >= 0) //check all sqaures surrounding co-ords of one supplied
      {
        if(mineDistance[xCheck][yCheck] == -1) //if mine here count
        {
          no++;
        }
      }
      xCheck++;
    }while(xCheck != x + 2);
    xCheck = x - 1;
    yCheck++;
  }while(yCheck != y + 2);
  return no;
}

void CalculateMineDistances(int mineDistance[xSize][ySize])//for al locations on board if no mine present assign value
{
  for(int x = 0; x < xSize; x++)
  {
    for(int y = 0; y < ySize; y++)
    {
      if(mineDistance[x][y] != -1)
      {
        mineDistance[x][y] = GetHowMany(x, y, mineDistance);
      }
    }
  }
}

int Menu()//Print and request choice for main menu
{
  int option = -1;

  printf("MENU \n\n");
  printf("1. Begin Game\n");
  printf("2. Instructions\n");
  printf("3. Exit\n\n");

  printf("Please pick an option: ");
  scanf("%i", &option);

  return option;
}

void Instructions()
{
  printf("\n\n");
  printf("INSTRUNCTIONS\n\n");
  printf("Enter the co-ordinates of the area you would like to flag for a mine "
    "or sweep.\nEnter F for flag and S for sweep.\nIf you wish to unflag enter U.\n"
    "Flag areas where you belive mines to be whilst sweeping other areas.\n"
    "Sweeping an area will inform you as to how many mines are nearby.\nThe game"
    " will be over when you either sweep a mine and loose\nor flag all mines and "
    "have sweept all other areas and won.\n\n");
}

void DrawBoard(char valueKnown[xSize][ySize])
{
  printf("     |");
  for(int x = 0; x < xSize; x++)//first row
  {
      if(x < 10)//if number one digit add extra space in draw so it all lines up
      {
        printf("  %i  |", x);
      }
      else
      {
        printf("  %i |", x);
      }
  }

  for(int y = 0; y < ySize; y++)
  {
    if(y < 10)
    {
      printf("\n  %i  |", y);
    }
    else
    {
      printf("\n  %i |", y);
    }
    for(int x = 0; x < xSize; x++)
    {
        printf("  %c  |", valueKnown[x][y]);
    }
  }
  printf("\n\n\n");
}

int GetGuessCoordinates(int *xCor, int *yCor, char valueKnown[xSize][ySize])
{
  printf("Guess co-ordinates (x, y): ");
  scanf(" %i, %i", xCor, yCor);
  while(getchar() != '\n');

  while(valueKnown[*xCor][*yCor] != 'X') //if co-ordinates aleady been displayed request new ones
  {
    if(*xCor >= xSize || *xCor < 0 || *yCor >= ySize || *yCor < 0)//if co-ordinates to big or small to fit on board request new ones
    {
      *xCor = -1;
      *yCor = -1;
      printf("Invalid co-ordinates, Please try again.\n");
      printf("Guess co-ordinates (x, y): ");
      scanf("%i, %i", xCor, yCor);
      while(getchar() != '\n');
    }
    else if(valueKnown[*xCor][*yCor] == 'F')//if a flag found here, check if user wants to unflag or if a mistake occured
    {
      char check;
      printf("\nThis location is flagged would you like to unflag it? (Y/N): ");
      scanf(" %c", &check);
      if(check == 'Y' || check == 'y')
      {
        printf("\n");
        return 0;
      }
      else //user said no to unflagging
      {
        *xCor = -1;
        *yCor = -1;
        printf("Please enter another set of co-ordinates: ");
        scanf("%i, %i", xCor, yCor);
      }
    }
    else
    {
      *xCor = -1;
      *yCor = -1;
      printf("Location value is known.\n");
      printf("Guess co-ordinates (x, y): ");
      scanf("%i, %i", xCor, yCor);
    }
  }

  return 1;
}

char GetTypeofGuess()//Find out if the user wants to flag or sweep co-ords
{
  char input;
  printf("Enter 'F' to place a flag, else press 'S' to sweep the area: ");
  scanf(" %c", &input);
  while(input != 'F' && input != 'S' && input != 's' && input != 'f')
  {
    printf("Invalid option, please try again.\n");
    printf("Enter 'F' to place a flag, else press 'S' to sweep the area: ");
    scanf(" %c", &input);
  }
  printf("\n");
  return input;
}

void GameOver()
{
  printf("SORRY YOU LOST! :(\n");
}

void DisplayMines(int mineDistance[xSize][ySize], char valueKnown[xSize][ySize]) //Show alll the mines on the board
{
  for(int x = 0; x < xSize; x++)
  {
    for(int y = 0; y < ySize; y++)
    {
      if(mineDistance[x][y] == -1)
      {
        valueKnown[x][y] = 'M';
      }
    }
  }
}

void ShowAllNearBy(int xCor, int yCor, char valueKnown[xSize][ySize], int mineDistance[xSize][ySize])//when sweep occurs if mine not find display value as well of all near by values
{
  int xCheck = xCor - 1;
  int yCheck = yCor - 1;
  do
  {
    do
    {
      if(xCheck < xSize && xCheck >= 0 && yCheck < ySize && yCheck >= 0)
      {
        if(mineDistance[xCheck][yCheck] == 0) // if 0 keep looking for others nearby
        {
          if(valueKnown[xCheck][yCheck] != ' ')//if not already known show its surroundings
          {
            valueKnown[xCheck][yCheck] = ' ';
            ShowAllNearBy(xCheck, yCheck, valueKnown, mineDistance);
          }
        }
        else
        {
          valueKnown[xCheck][yCheck] = mineDistance[xCheck][yCheck] + '0'; //set the number of mines nearby value to be displayed
        }
      }
      xCheck++;
    } while(xCheck != xCor + 2);
    xCheck = xCor - 1;
    yCheck++;
  } while(yCheck != yCor + 2);
}

int CheckIfWon(char valueKnown[xSize][ySize], int mineDistance[xSize][ySize]) //if the number of flags is equal to the number of ammounts and no unchecked areas
{
  int amount = 0;
  for(int x = 0; x < xSize; x++)
  {
    for(int y = 0; y < ySize; y++)
    {
      if(valueKnown[x][y] == 'X') //if unchecked location found return 0 for not won
      {
        return 0;
      }
      if(valueKnown[x][y] == 'F' && mineDistance[x][y] == -1) //if location is flagged and mine is there count, if flagged but no mine, amount won't be equal to mineAmount and will not trigger gameover
      {
        amount++;
      }
    }
  }
  return amount;
}

void DisplayKey()
{
  printf("X = No action has been performed at said location \n");
  printf("F = Location has been flagged\n");
  printf("\n");
}

void PickBoardSize() //if custom difficulty is chosen allow user to pick the board size
{
  printf("Please enter the dimensions of the board(width x height): ");
  scanf("%i x %i", &xSize, &ySize);
  while(getchar() != '\n');

  while(xSize <= 9 || ySize <= 9 || xSize > MAXSIZE || ySize > MAXSIZE) //has to be within these limits to fit on command line neatly
  {
    printf("Invalid dimensions please try again. The maximum size of the board is %i x %i and minimum size is 10 x 10\n", MAXSIZE, MAXSIZE);
    scanf("%i x %i", &xSize, &ySize);
    while(getchar() != '\n');
  }
}

float PickMinePercent() //allows the user who picked custom difficulty to pick the percentage of the board which will have mines
{
  int amount = 0;

  printf("Please enter the percentage of mines you want your board to contain (10-99%%): \n");
  scanf("%i", &amount);
  while(getchar() != '\n');

  while(amount <= 10 || amount > 99) //has to have at least 10% and not a full board
  {
    printf("Invalid value. Please try again.");
    scanf("%i", &amount);
    while(getchar() != '\n');
  }
  return (float)amount / 100;
}

int LevelMenu() //get diffuclty choice
{
  int option = 0;

  printf("\nDIFFICULTY MENU\n");
  printf("\n1. Easy\n");
  printf("2. Intermediate\n");
  printf("3. Hard\n");
  printf("4. Custom\n");

  scanf("%i", &option);
  return option;
}

void getLevel()//depending on difficulty chosen set the board and mine parameters
{
  int option;
  srand(time(NULL));
  float offset = 0.0;
  float minimum = 0.0;
  do
  {
    option = LevelMenu();
    if(option == 1)
    {
        xSize = 10;
        ySize = 10;
        offset = 0.1;
        minimum = 0.1;
    }
    else if(option == 2)
    {
      xSize = 20;
      ySize = 20;
      offset = 0.1;
      minimum = 0.2;
    }
    else if(option == 3)
    {
      xSize = MAXSIZE;
      ySize = MAXSIZE;
      offset = 0.15;
      minimum = 0.3;
    }
    else if(option == 4)
    {
      PickBoardSize();
      mineAmount = PickMinePercent() * (xSize * ySize);
    }
    else
    {
      printf("Invalid option, please try again\n");
    }

  } while(option < 1 || option > 4);//while not valid option supplied keep asking
  if(option != 4)
  {
    mineAmount = (rand() % (int)((xSize * ySize) * offset)) + (minimum * xSize * ySize);
  }
}


int main()
{
  int option = -1;;

  while (1)
  {
    option = Menu();//get menu option
    if(option == 1)// if begin game
    {
      getLevel(); //get difficulty
      char valueKnown[xSize][ySize]; //known values 'X', ' ', 'F'
      int mineDistance[xSize][ySize];
      int won = 0;//game won state
      int xCor;
      int yCor;
      char guess;
      InitalizeKnownGrid(valueKnown);
      InitalizeMineDistances(mineDistance);
      PlaceMines(mineAmount, mineDistance);
      CalculateMineDistances(mineDistance);
      printf("\n\n");
      printf("                 LET'S BEGIN! \n\n");
      DisplayKey();
      while(!won)//while game in progress keep drawing board and requesting co-ordinates
      {
        xCor = -1;
        yCor = -1;
        DrawBoard(valueKnown);
        int ask = GetGuessCoordinates(&xCor, &yCor, valueKnown);
        if(ask == 1)//if user did not unflag a location
        {
          guess = GetTypeofGuess();
          if(guess == 'F' || guess == 'f')//if user chose to flag
          {
            valueKnown[xCor][yCor] =  'F';
          }
          else //user chose to sweep
          {
            if(mineDistance[xCor][yCor] == -1) // if mine where chosen to sweep game over you loose
            {
              DisplayMines(mineDistance, valueKnown); //draw end state board showing all mine positions
              DrawBoard(valueKnown);
              GameOver();
              won = 1;
            }
            else if(mineDistance[xCor][yCor] == 0)//if sweep where no nearby mines show all near by values
            {
              valueKnown[xCor][yCor] = ' ';
              ShowAllNearBy(xCor, yCor, valueKnown, mineDistance);//output all around the zero and recurisve along other zeros
            }
            else //show number of near by mines
            {
              valueKnown[xCor][yCor] = mineDistance[xCor][yCor] + '0';
            }
          }
          int amount = CheckIfWon(valueKnown, mineDistance); //check if the user is in win state
          if(amount == mineAmount)// all mines flagged and correct number of flags, change to win state
          {
            won = 1;
            DrawBoard(valueKnown);
            printf("Congratualtions! YOU WON!\n");
          }
        }
        else //if user chose to unflag revert back to unknown state
        {
          valueKnown[xCor][yCor] = 'X';
        }
      }
    }
    else if(option == 2)
    {
      Instructions();
    }
    else if(option == 3)
    {
      exit(1);
    }
    else //no valid menu input
    {
      printf("Invalid input please try again\n");
    }
  }
  return 0;
}
